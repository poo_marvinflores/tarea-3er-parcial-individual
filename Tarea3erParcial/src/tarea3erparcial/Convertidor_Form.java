/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea3erparcial;
public class Convertidor_Form extends javax.swing.JFrame {

    /**
     * Creates new form Convertidor_Form
     */
    Conversiones conv=new Conversiones();
    public Convertidor_Form() {
        initComponents();
        this.setSize(630,240);
        this.setTitle("Convertidor de Monedas");
        this.tx2.setEditable(false);
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated b y the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tx1 = new javax.swing.JTextField();
        tx2 = new javax.swing.JTextField();
        cb2 = new javax.swing.JComboBox<>();
        cb1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("CONVERTIDOR DE MONEDAS");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 20, 618, 36);

        tx1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        getContentPane().add(tx1);
        tx1.setBounds(10, 83, 181, 50);

        tx2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        getContentPane().add(tx2);
        tx2.setBounds(10, 139, 181, 50);

        cb2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        cb2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dolar - $", "Euro - €", "Soles - S/.", "Lempiras - L.", "Libras Esterlinas - ‎£" }));
        getContentPane().add(cb2);
        cb2.setBounds(209, 139, 274, 50);

        cb1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        cb1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dolar - $", "Euro - €", "Soles - S/.", "Lempiras - L.", "Libras Esterlinas - ‎£" }));
        getContentPane().add(cb1);
        cb1.setBounds(209, 82, 274, 50);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("CALCULAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(490, 80, 110, 110);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(tx1.getText().equals("")){
            return;
        }
            this.calcular(0);
            this.calcular(1);
            this.calcular(2);
            this.calcular(3);
            this.calcular(4);
                                
    }//GEN-LAST:event_jButton1ActionPerformed
public void calcular(int v){
           if(cb1.getSelectedIndex()==v && cb1.getSelectedIndex()==0){
            //System.out.println(conv.monedas_USA(cb1.getSelectedIndex()));
            tx2.setText(""+(Double.parseDouble(tx1.getText())*conv.monedas_USA(cb2.getSelectedIndex())));
        }else if(cb1.getSelectedIndex()==v && cb1.getSelectedIndex()==1){
           // System.out.println(conv.monedas_EST(cb1.getSelectedIndex()));
            tx2.setText(""+(Double.parseDouble(tx1.getText())*conv.monedas_EU(cb2.getSelectedIndex())));
        }
        else if(cb1.getSelectedIndex()==v && cb1.getSelectedIndex()==2){
           // System.out.println(conv.monedas_EST(cb1.getSelectedIndex()));
            tx2.setText(""+(Double.parseDouble(tx1.getText())*conv.monedas_PE(cb2.getSelectedIndex())));
        }
        else if(cb1.getSelectedIndex()==v && cb1.getSelectedIndex()==3){
           // System.out.println(conv.monedas_EST(cb1.getSelectedIndex()));
            tx2.setText(""+(Double.parseDouble(tx1.getText())*conv.monedas_HN(cb2.getSelectedIndex())));
        }else if(cb1.getSelectedIndex()==v && cb1.getSelectedIndex()==4){
           // System.out.println(conv.monedas_EST(cb1.getSelectedIndex()));
            tx2.setText(""+(Double.parseDouble(tx1.getText())*conv.monedas_EST(cb2.getSelectedIndex())));
        }
}
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Convertidor_Form().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cb1;
    private javax.swing.JComboBox<String> cb2;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField tx1;
    private javax.swing.JTextField tx2;
    // End of variables declaration//GEN-END:variables
}
